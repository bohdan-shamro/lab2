import numpy as np
import scipy.misc as scipy
import matplotlib.pyplot as plt

def f(x):
    return x**2+5*np.sin(x)
def f_derivative(x):
    return scipy.derivative(f, x)
    #return 2*x+5*np.cos(x)

x = np.arange(-8, 8,0.01)
plt.plot(x, f(x))
plt.xlabel('x label')
plt.ylabel('y label')
plt.show()

def get_alpha(t):
    return p1/(p+p2)

def gradient(p,stepsx,stepsy,epoch,previous_x,current_x): 
    for i in range(epoch):
        alpha = get_alpha(p)
        current_x = previous_x - alpha * f_derivative(previous_x)
        previous_x = current_x
        p+=1
        stepsx.append(previous_x)
        stepsy.append(f(previous_x))
    return previous_x


epoch=100000
p,p1,p2=0,10,10000
stepsx=[]
stepsy=[]
previous_x, current_x = 0, 0
    
previous_x=gradient(p,stepsx,stepsy,epoch,previous_x,current_x)
    
x = np.arange(-10, 10,0.1)
plt.plot(x, f(x))
plt.scatter(stepsx,stepsy,c="g",label="Steps")
plt.scatter(previous_x,f(previous_x),c="r",label="Minimum")
plt.annotate('Minimum', xy=(previous_x, f(previous_x)), xytext=(previous_x - 5, f(previous_x) + 5),arrowprops=dict(facecolor='grey', shrink=0.001))
plt.legend()
plt.show()
print("x_min = ", previous_x, "f(x_min) = ", f(previous_x))